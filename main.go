package main

import (
	"fmt"
	"math/rand"
	"net/http"
	"os"
	"time"
)

func getEndpoint() string {
	return os.Getenv("TRADER_ENDPOINT")
}
func triggerEvent() {
	_, err := http.Get(getEndpoint())
	if err != nil {
		panic(fmt.Errorf("Can't trigger event: %w", getEndpoint()))
	}
}

func main() {
	tick := time.Tick(1 * time.Second)
	for {
		select {
		case <-tick:
			fmt.Printf("tick\n")
			if rand.Intn(2) == 1 {
				fmt.Println("event")
				triggerEvent()
			}
		}
	}
}
